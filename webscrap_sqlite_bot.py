from bs4 import BeautifulSoup
import requests
import os
import time
import sqlite3
import re

start = time.time()

tag_id = "Link--primary"
# bot and chat ids
token = ""
chat_id = ""

def send_message(message):

    apiToken = token
    chatID = chat_id
    apiURL = f'https://api.telegram.org/bot{apiToken}/sendMessage'

    try:
        response = requests.post(apiURL, json={'chat_id': chatID, 'text': message})
        print(response.text)
    except Exception as e:
        print(e)

# connect to database
db = sqlite3.connect('site_crawl.db')
cursor = db.cursor()

# get the URL to crawl
url = input("URL to crawl: ")
if len(url) < 1:
    # url = "https://canodrom.barcelona"
    print(url)

def get_db_name(url):
    """Takes a URL and strips it to use as a table name"""
    if 'www' in url:
        url_clense = re.findall('ht.*://www\.(.*?)\.',url)
        url_clense = url_clense[0].capitalize()
        return url_clense
    else:
        url_clense = re.findall('ht.*://(.*?)\.',url)
        url_clense = url_clense[0].capitalize()
        return url_clense

db_name = get_db_name(url)


# Create database

cursor.execute("CREATE TABLE IF NOT EXISTS " + db_name + " (URLID INTEGER PRIMARY KEY AUTOINCREMENT, Release UNIQUE, Time TIMESTAMP DEFAULT CURRENT_TIMESTAMP)")

#Insert releases into table

def insert_data():
    website_request = requests.get(url, timeout=5)
    website_content = BeautifulSoup(website_request.content, 'html.parser')
    release = [release.text for release in website_content.find_all(class_ = "Link--primary")]
    release = [(s,) for s in release]
    cursor.executemany("INSERT OR IGNORE INTO " + db_name + " (Release) VALUES(?)", release)
    db.commit()

insert_data()

# check if there were new jobs added

def check_result_send_mess():
    jobs_link_pm = crawling(url,tag_id)

    db = sqlite3.connect('site_crawl.db')
    cursor = db.cursor()    

    for item in jobs_link_pm:
        job_exists = cursor.execute('SELECT Release FROM' + db_name + 'WHERE Release = %s', [item.text])

        if len(cursor.fetchall()) != 1:
            mess_content = item.text + item['href']
            send_message(mess_content)
            cursor.execute('INSERT INTO' + db_name + '(Release) VALUES (%s);', [item.text])
            db.commit()
        else:
            continue

    cursor.close()


cursor.close()
db.close()
end = time.time()
print(end - start)