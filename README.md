# webscrap_bot

## Description

This script is originally created to check for changes on github repository and if there is any change recieve a ntification on telegram

## Dependencies

`pip install -r requirements.txt`

## Requirements

- Small container, vps or raspberrypi to run the script
- A telegram bot token and chat_id where to receive notifications
